package com.hadadas.customer;

import com.hadadas.customer.entities.Customer;
import com.hadadas.customer.repos.CustomerRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

@SpringBootTest
class CustomerApplicationTests {

	@Autowired
	private CustomerRepository customerRepository;

	@Test
	public void createCustomer(){
		Customer customer = new Customer();
		customer.setEmail("maorhadad@gmail.com");
		customer.setName("maor");
		customerRepository.save(customer);

	}

	@Test
	public void findStudentById(){
		Optional<Customer> customer = customerRepository.findById(1L);
		if(customer.isPresent()){
			System.out.println(customer.get().toString());

		}
	}

	@Test
	public void updateCustomerById(){
		Optional<Customer> customer = customerRepository.findById(1L);
		if(customer.isPresent()){
			customer.get().setName("maor hadad");
			customerRepository.save(customer.get());
		}
	}

	@Test
	public void testDeleteCustomer(){
		customerRepository.deleteById(1L);
	}

}
